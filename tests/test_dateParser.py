from unittest import TestCase
from src.date_parser import DateParser
import logging


class TestDateParser(TestCase):

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.logger = logging.getLogger('TestDateParser')

    def test_find_year_assert_true(self):
        self.logger.warning(f'start test: find_year_assert_true\n')
        # given
        input_test_1 = [1, 2, 3]
        output_test_1 = '2001-02-03'
        input_test_2 = [31, 34, 3]
        output_test_2 = '2034-03-31'
        input_test_3 = [99, 20, 3]
        output_test_3 = '2099-03-20'
        input_test_4 = [3, 20, 1]
        output_test_4 = '2001-03-20'
        input_test_5 = [3, 88, 1]
        output_test_5 = '2088-01-03'
        input_test_6 = [31, 2988, 1]
        output_test_6 = '2988-01-31'
        input_test_7 = [5, 12, 13]
        output_test_7 = '2005-12-13'
        input_test_8 = [17, 5, 1]
        output_test_8 = '2001-05-17'
        input_test_9 = [8, 29, 7]
        output_test_9 = '2007-08-29'
        input_test_10 = [11, 32, 30]
        output_test_10 = '2032-11-30'
        input_test_11 = [11, 0, 30]
        output_test_11 = '2000-11-30'
        input_test_12 = [11, 12, 13]
        output_test_12 = '2011-12-13'
        input_test_13 = [12, 13, 14]
        output_test_13 = '2013-12-14'
        input_test_14 = [2, 30, 28]
        output_test_14 = '2030-02-28'
        input_test_15 = [4, 30, 31]
        output_test_15 = '2031-04-30'
        input_test_16 = [2, 28, 29]  # leap year
        output_test_16 = '2028-02-29'
        input_test_17 = [12, 31, 30]
        output_test_17 = '2030-12-31'
        input_test_18 = [2, 31, 27]
        output_test_18 = '2031-02-27'
        input_test_19 = [2, 31, 11]
        output_test_19 = '2031-02-11'

        # when
        year_1 = DateParser.find_correct_date_from_elements(input_test_1)
        year_2 = DateParser.find_correct_date_from_elements(input_test_2)
        year_3 = DateParser.find_correct_date_from_elements(input_test_3)
        year_4 = DateParser.find_correct_date_from_elements(input_test_4)
        year_5 = DateParser.find_correct_date_from_elements(input_test_5)
        year_6 = DateParser.find_correct_date_from_elements(input_test_6)
        year_7 = DateParser.find_correct_date_from_elements(input_test_7)
        year_8 = DateParser.find_correct_date_from_elements(input_test_8)
        year_9 = DateParser.find_correct_date_from_elements(input_test_9)
        year_10 = DateParser.find_correct_date_from_elements(input_test_10)
        year_11 = DateParser.find_correct_date_from_elements(input_test_11)
        year_12 = DateParser.find_correct_date_from_elements(input_test_12)
        year_13 = DateParser.find_correct_date_from_elements(input_test_13)
        year_14 = DateParser.find_correct_date_from_elements(input_test_14)
        year_15 = DateParser.find_correct_date_from_elements(input_test_15)
        year_16 = DateParser.find_correct_date_from_elements(input_test_16)
        year_17 = DateParser.find_correct_date_from_elements(input_test_17)
        year_18 = DateParser.find_correct_date_from_elements(input_test_18)
        year_19 = DateParser.find_correct_date_from_elements(input_test_19)

        # then
        self.assertEqual(output_test_1, year_1, msg='failed input_test_1')
        self.assertEqual(output_test_2, year_2, msg='failed input_test_2')
        self.assertEqual(output_test_3, year_3, msg='failed input_test_3')
        self.assertEqual(output_test_4, year_4, msg='failed input_test_4')
        self.assertEqual(output_test_5, year_5, msg='failed input_test_5')
        self.assertEqual(output_test_6, year_6, msg='failed input_test_6')
        self.assertEqual(output_test_7, year_7, msg='failed input_test_7')
        self.assertEqual(output_test_8, year_8, msg='failed input_test_8')
        self.assertEqual(output_test_9, year_9, msg='failed input_test_9')
        self.assertEqual(output_test_10, year_10, msg='failed input_test_10')
        self.assertEqual(output_test_11, year_11, msg='failed input_test_11')
        self.assertEqual(output_test_12, year_12, msg='failed input_test_12')
        self.assertEqual(output_test_13, year_13, msg='failed input_test_13')
        self.assertEqual(output_test_14, year_14, msg='failed input_test_14')
        self.assertEqual(output_test_15, year_15, msg='failed input_test_15')
        self.assertEqual(output_test_16, year_16, msg='failed input_test_16')
        self.assertEqual(output_test_17, year_17, msg='failed input_test_17')
        self.assertEqual(output_test_18, year_18, msg='failed input_test_18')
        self.assertEqual(output_test_19, year_19, msg='failed input_test_19')

    def test_find_year_assert_exception(self):
        self.logger.warning(f'start test: find_year_assert_exception\n')
        # given
        input_test_1 = [133, 2, 3]
        input_test_2 = []
        input_test_3 = [133, 2, 3, 4]

        # when / then
        with self.assertRaises(AttributeError):
            DateParser.find_correct_date_from_elements(input_test_1)
        with self.assertRaises(AttributeError):
            DateParser.find_correct_date_from_elements(input_test_2)
        with self.assertRaises(AttributeError):
            DateParser.find_correct_date_from_elements(input_test_3)

    def test_find_month_true(self):
        self.logger.warning(f'start test: find_month_true\n')
        # given
        input_test_1 = [1, 2]
        output_test_1 = '01'
        input_test_2 = [12, 13]
        output_test_2 = '12'
        input_test_3 = [30, 2]
        output_test_3 = '02'

        # when
        month_1 = DateParser.find_month(input_test_1)
        month_2 = DateParser.find_month(input_test_2)
        month_3 = DateParser.find_month(input_test_3)

        # then
        self.assertEqual(output_test_1, month_1)
        self.assertEqual(output_test_2, month_2)
        self.assertEqual(output_test_3, month_3)

    def test_find_month_exception(self):
        self.logger.warning(f'start test: find_month_exception\n')
        # given
        input_test_1 = [0, 2]
        input_test_2 = [12]
        input_test_3 = [30, 31]

        # when / then
        with self.assertRaises(AttributeError):
            DateParser.find_month(input_test_1)
        with self.assertRaises(AttributeError):
            DateParser.find_month(input_test_2)
        with self.assertRaises(AttributeError):
            DateParser.find_month(input_test_3)

    def test_find_day_true(self):
        self.logger.warning(f'start test: find_day_true\n')
        # given
        input_test_1 = [1]
        output_test_1 = '01'
        input_test_2 = [20]
        output_test_2 = '20'
        input_test_3 = [27]
        output_test_3 = '27'
        month = 2
        year = 2020

        # when
        day_1 = DateParser.find_day(input_test_1, month, year)
        day_2 = DateParser.find_day(input_test_2, month, year)
        day_3 = DateParser.find_day(input_test_3, month, year)

        # then
        self.assertEqual(output_test_1, day_1)
        self.assertEqual(output_test_2, day_2)
        self.assertEqual(output_test_3, day_3)

    def test_find_day_exception(self):
        self.logger.warning(f'start test: find_day_exception\n')
        # given
        input_test_1 = [0]
        input_test_2 = []
        input_test_3 = [32]
        month = 2
        year = 2020

        # when / then
        with self.assertRaises(AttributeError):
            DateParser.find_day(input_test_1, month, year)
        with self.assertRaises(AttributeError):
            DateParser.find_day(input_test_2, month, year)
        with self.assertRaises(AttributeError):
            DateParser.find_day(input_test_3, month, year)

    def test_is_leap_year_true(self):
        self.logger.warning(f'start test: _is_leap_year_true\n')
        # given
        year_1 = 2000
        year_2 = 1600
        year_3 = 2004
        year_4 = 2028
        year_5 = 2032

        # when
        result_1 = DateParser.is_leap_year(year_1)
        result_2 = DateParser.is_leap_year(year_2)
        result_3 = DateParser.is_leap_year(year_3)
        result_4 = DateParser.is_leap_year(year_4)
        result_5 = DateParser.is_leap_year(year_5)

        # then
        self.assertTrue(result_1)
        self.assertTrue(result_2)

    def test_is_leap_year_false(self):
        self.logger.warning(f'start test: _is_leap_year_false\n')
        # given
        year_1 = 1700
        year_2 = 2100
        year_3 = 1800
        year_4 = 1900

        # when
        result_1 = DateParser.is_leap_year(year_1)
        result_2 = DateParser.is_leap_year(year_2)
        result_3 = DateParser.is_leap_year(year_3)
        result_4 = DateParser.is_leap_year(year_4)

        # then
        self.assertFalse(result_1)
        self.assertFalse(result_2)
        self.assertFalse(result_3)
        self.assertFalse(result_4)
