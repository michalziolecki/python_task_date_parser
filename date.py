#!/usr/bin/python3

from src.date_controller import DateController

import sys
import os


def main(argv):
    if len(argv) > 1:
        print('To many arguments!\nrun by: date.py <inputfile>')
    arg = argv.pop()
    if '-h' in arg or '--help' in arg:
        print('date.py <inputfile>')
        sys.exit(0)
    if os.path.exists(arg):
        print("Python Task:\n - Date Parser")
        date_controller = DateController(arg)
        date_controller.run_parser()
    else:
        print('File not exists or bad parameter!\nrun by: date.py <inputfile>')


if __name__ == "__main__":
    main(sys.argv[1:])

