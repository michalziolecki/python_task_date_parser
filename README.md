# Python Task #

Exercise for MiloSolutions

### Date Parser ###

### Requirements ###
* Python: 3.6+

### Run application command ###
* Windows: 
     `python3 date.py <file_path>`
* Linux:
    `python3 date.py <file_path>`
    `date.py <file_path>`

### Run tests command ###
* Windows: 
    `python3 test_runner.py`
* Linux: 
    `python3 test_runner.py`

### Source path ###
* src/

### Tests path ###
* tests/

### Example inputs ###
* 1/2/3
* 3/20/1
* 31/12/3000
* 31/12/2999
* 31/12/99
* 11/12/2999
* 3/20/21

