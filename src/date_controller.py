from src.file_reader import FileReader
from src.date_parser import DateParser


class DateController:

    def __init__(self, path: str):
        self.__source = path

    def run_parser(self):
        print('Parser run')
        with FileReader(self.__source) as fr:
            generator = fr.read_line_by_line_lazy()
            for line in generator:
                DateController.__pars_line(line)
        print('Parser stop')

    @classmethod
    def __pars_line(cls, line):
        try:
            date = DateParser.parse_date(line)
            print(f'{line} => {date}')
        except (AttributeError, ValueError):
            print(f'{line} => is illegal')
