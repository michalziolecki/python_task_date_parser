import os
from typing import Generator


class FileReader:
    def __init__(self, path=""):
        self.__source = path

    def __enter__(self):
        self.__file_descriptor = open(self.source)
        return self

    def __exit__(self, *args):
        self.__file_descriptor.close()

    @property
    def source(self) -> str:
        return self.__source

    @source.setter
    def source(self, path: str):
        self.__source = path

    @staticmethod
    def __lazy_read(path: str) -> Generator[str, None, None]:
        with open(path, 'r') as fd:
            for line in fd.readlines():
                yield line

    @staticmethod
    def __check_path(path: str):
        if not path:
            raise FileExistsError("Empty path!")
        if not os.path.exists(path):
            raise FileExistsError("File not exist!")

    @staticmethod
    def read_lines_lazy(path: str) -> Generator[str, None, None]:
        FileReader.__check_path(path)
        return FileReader.__lazy_read(path)

    @staticmethod
    def read_first_line_static(path: str) -> str:
        line = ""
        with open(path, 'r') as fd:
            for char in fd.readline():
                line += char
        return line

    @staticmethod
    def __cut_new_line_char(line: str) -> str:
        if line.endswith('\n'):
            line = line[:-1]
        return line

    def read_line_by_line_lazy(self) -> Generator[str, None, None]:
        FileReader.__check_path(self.source)
        for line in self.__file_descriptor.readlines():
            line = FileReader.__cut_new_line_char(line)
            yield line

    def read_all_lines_list(self) -> list:
        content = []
        for line in self.__file_descriptor.readlines():
            line = FileReader.__cut_new_line_char(line)
            content.append(line)
        return content

    def read_first_line(self) -> str:
        line = ""
        for char in self.__file_descriptor.readline():
            if char != '\n':
                line += char
        return line
