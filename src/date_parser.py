from collections import namedtuple
from copy import copy
# from datetime import datetime
import datetime

Constants = namedtuple('Constants', ['MIN_YEAR', 'MAX_YEAR', 'MIN_MONTH', 'MAX_MONTH', 'MIN_DAY', 'MAX_DAYS'])
days = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31,
}
const = Constants(2000, 2999, 1, 12, 1, days)


class DateParser:
    """
        Assumptions:
         - day 1 - "${days}" values
         - month 1 - 12 values
         - year 2000 - 2999 values
    """

    @staticmethod
    def parse_date(input_date: str) -> str:
        elements: list = DateParser.__prepare_input(input_date)
        return DateParser.find_correct_date_from_elements(elements)

    @classmethod
    def __prepare_input(cls, input_date: str) -> list:
        elements = []
        elements.extend(input_date.split("/"))
        elements = [int(_object) for _object in elements]
        return elements

    @classmethod
    def __find_date_by_combination(cls, elements: list) -> str:
        """
        Looking for earliest date 20XX-XX-XX-> year = 20XX < 2031
        :param elements:
        :return str:
        """

        # exist 6 possible dates because => 3!/(3-3)!
        dates = []
        for it_0 in range(3):
            it_1 = 0
            it_2 = 0
            if it_0 == 0:
                it_1 = 1
                it_2 = 2
            elif it_0 == 1:
                it_2 = 2
            else:  # it_0 == 2
                it_2 = 1
            try:
                date = datetime.date(DateParser.__concatenate_year(elements[it_0]), elements[it_1], elements[it_2])
                dates.append(date)
            except ValueError:
                pass
            try:
                date = datetime.date(DateParser.__concatenate_year(elements[it_0]), elements[it_2], elements[it_1])
                dates.append(date)
            except ValueError:
                pass

        if not dates:
            raise ValueError("Incorrect date!")

        min_date = dates.pop()
        for date in dates:
            if min_date > date:
                min_date = date

        return str(min_date)

    @classmethod
    def __concatenate_year(cls, value: int) -> int:
        concatenation = {
            1: f'200{value}',
            2: f'20{value}',
            3: f'2{value}',
            4: f'{value}'
        }
        val_len = len(str(value))
        if val_len > 4:
            raise ValueError('This values can not be concatenate!')
        return int(concatenation.get(val_len))

    @classmethod
    def __get_lowest_from_list(cls, objects: list, elements=None) -> int:
        objects.sort()
        objects.reverse()
        year: int = objects.pop()
        if elements:
            elements.remove(year)
        return year

    @staticmethod
    def find_correct_date_from_elements(elements: list) -> str:

        if not len(elements) == 3:
            raise AttributeError("Incorrect number of parameters on input!")

        # looking for earliest date XXXX-XX-XX-> (year is 20XX > 2031 and 2XXX < 2999) or year is 2000
        result = DateParser.__find_basic_year_then_rest(elements)
        if result:
            return result

            # looking for earliest date 20XX-XX-XX-> year = 20XX < 2031
        return DateParser.__find_date_by_combination(elements)

    @classmethod
    def __find_basic_year_then_rest(cls, elements: list):

        def concatenate_date(year_: int, elements_: list) -> str:
            year_: int = DateParser.__concatenate_year(year_)
            DateParser.__check_final_year(year_)
            month_ = DateParser.find_month(elements_)
            day_ = DateParser.find_day(elements, int(month_), year)
            return f'{year_}-{month_}-{day_}'

        year_l: list = [element for element in elements if element > 31]
        if year_l:
            # find year > 2031
            year: int = DateParser.__prepare_found_year(year_l, elements)
            if 99 < year < 1000:  # Assumption not allowed - three digit on input XXX - year 2XXX
                raise AttributeError('Incorrect input ! Assumption not allowed (three digit on input XXX - year 2XXX)')
            return concatenate_date(year, elements)

        year_l: list = [element for element in elements if element == 0]
        if year_l:
            # find year == 2000
            year: int = DateParser.__prepare_found_year(year_l, elements)
            return concatenate_date(year, elements)

        return None

    @classmethod
    def __check_final_year(cls, year: int):
        if not const.MIN_YEAR <= year <= const.MAX_YEAR:
            raise AttributeError('Not allowed year!')

    @classmethod
    def __prepare_found_year(cls, year_l: list, elements: list) -> int:
        DateParser.__correct_found_year_position(year_l)
        year = year_l.pop()
        elements.remove(year)
        return int(year)

    @classmethod
    def __correct_found_year_position(cls, year_l: list):
        if not year_l or len(year_l) > 1:
            raise AttributeError('Found more than one position of year')

    @staticmethod
    def find_month(elements: list) -> str:
        def concatenate_month(value: int) -> str:
            concatenation = {
                2: f'{value}',
                1: f'0{value}'
            }
            val_len = len(str(value))
            if val_len > 2:
                raise ValueError('This values can not be concatenate!')
            return concatenation.get(val_len)

        if not len(elements) == 2:
            raise AttributeError('Incorrect number of parameters on input!')

        month = DateParser.__get_lowest_from_list(objects=elements)
        if not const.MIN_MONTH <= month <= const.MAX_MONTH:
            raise AttributeError(f'Incorrect number of parameter month on input! Found value: {month}')

        return concatenate_month(month)

    @staticmethod
    def find_day(elements: list, month: int, year: int) -> str:
        def concatenate_day(value: int) -> str:
            concatenation = {
                2: f'{value}',
                1: f'0{value}'
            }
            val_len = len(str(value))
            if val_len > 2:
                raise ValueError('This values can not be concatenate!')
            return concatenation.get(val_len)

        if not len(elements) == 1:
            raise AttributeError("Incorrect number of parameters on input!")

        day = elements.pop()
        if not DateParser.__is_correct_day(day, month, year):
            raise AttributeError(f'Incorrect number of parameter day on input! Found value: {day}')

        return concatenate_day(day)

    @classmethod
    def __is_correct_day(cls, day: int, month: int, year: int) -> bool:
        max_days: int = const.MAX_DAYS.get(month)
        if DateParser.is_leap_year(year) and month == 2:
            max_days += 1

        if const.MIN_DAY <= day <= max_days:
            return True
        return False

    @staticmethod
    def is_leap_year(year: int) -> bool:
        is_leap = False
        if (year % 4 == 0 and year % 100 != 0) or year % 400 == 0:
            is_leap = True
        return is_leap
