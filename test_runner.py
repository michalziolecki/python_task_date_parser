import unittest
import os

loader = unittest.TestLoader()
root = os.path.dirname(os.path.abspath(__file__))
start_dir = f'{root}/tests'
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()
runner.run(suite)
